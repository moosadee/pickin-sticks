#include <allegro.h>
#include <cstdlib>
#include <ctime>
#include "ClassAlec.h"
#include "ClassStick.h"
#include "MiscFunctions.h"

/*
Version 1.1 --
- Music speeds up when you collect (score % 10 = 0) sticks
- Alec also speeds up every 10 sticks
- Added shitty ghost trail when you're going fast enough

Version 1.0--
I'm sorry that my source code is poop.  I'll
try harder when I make a nicer game. :D
*/

int main(int argc, char *argv[])
{
  initializeCrap();
  install_joystick(JOY_TYPE_AUTODETECT);

  //Various things that are a pain to pass to another function. :D
  BITMAP *buffer = create_bitmap(640,480);
  BITMAP *bmpMainMenu = load_bitmap("MainMenuBG.bmp", NULL);
  BITMAP *bmpAbout = load_bitmap("aboutwindow.bmp",NULL);
  BITMAP *bmpGameBg = load_bitmap("grass.bmp",NULL);
  BITMAP *bmpAlec = load_bitmap("Alec.bmp",NULL);
  BITMAP *bmpStick = load_bitmap("stick.bmp",NULL);
  //MIDI *midSong = load_midi("RPGMAKERarena.mid");
  SAMPLE *samSong = load_sample("RPGMAKErarena.wav");
  SAMPLE *samScore = load_sample("RPGMAKERJingle2.wav");
  int song_frequency = 1000;
  bool fullscreen = true;

  bool done = false;    //This is the thing, you know, to tell the program if it should close now or not.

  //Lollerwaffle, an array of one stick.  I was gonna put two, but you made me mad
  //so I killed stick[1] and now you only have stick[0]. :P
  Alec aPlayer;
  Stick stick[1];
  stick[0].generateNewCoordinates();

  //Which screen you're on!  Screen 1 = Menu, Screen 2 = About, Screen 3 = Game
  int iScreen = 1;

  while (!done)   //While not done (aka While (done != true)
  {
    while (speed_counter > 0)
    {
        if (key[KEY_F5])
        {
            if ( fullscreen ) { set_gfx_mode(GFX_AUTODETECT_WINDOWED, 640, 480, 0, 0); fullscreen = false; }
            else { set_gfx_mode(GFX_AUTODETECT, 640, 480, 0, 0); fullscreen = true; }
        }
        else if (key[KEY_F4]) { done = true; }
      if (iScreen == 1)       //Menu screen!
      {
          poll_joystick();
        if (key[KEY_1] || joy[0].button[0].b) { iScreen = 3; play_sample(samSong, 255, 128, song_frequency, true); }   //Start game (go to screen 3), play it's bg song.
        else if (key[KEY_2]) {  iScreen = 2; }                    //Go to about screen
        else if (key[KEY_3]) { done = true; }                     //Quit
      }
      else if (iScreen == 2)  //About screen!
      {
        if (key[KEY_ESC])     //If you press esc, go back to menu screen.
        {
          iScreen = 1;
        }
      }
      else if (iScreen == 3)  //Game screen!
      {
          poll_joystick();
        if (key[KEY_DOWN] || key[KEY_S]
                || joy[0].stick[0].axis[1].d2) {
                    if (joy[0].button[0].b) { aPlayer.move(2); }
                    if (joy[0].button[2].b) { aPlayer.move(2); }
                    aPlayer.move(2);
                    aPlayer.update_prev();
                }               //the integer is which direction to move.  Look at the numpad to figure out.
        else if (key[KEY_LEFT] || key[KEY_A]
                || joy[0].stick[0].axis[0].d1) {
                    if (joy[0].button[0].b) { aPlayer.move(4); }
                    if (joy[0].button[2].b) { aPlayer.move(4); }
                    aPlayer.move(4);
                    aPlayer.update_prev();
                    }
        else if (key[KEY_RIGHT] || key[KEY_D]
                || joy[0].stick[0].axis[0].d2) {
                    if (joy[0].button[0].b) { aPlayer.move(6); }
                    if (joy[0].button[2].b) { aPlayer.move(6); }
                    aPlayer.move(6);
                    aPlayer.update_prev();
                    }
        else if (key[KEY_UP] || key[KEY_W]
                || joy[0].stick[0].axis[1].d1) {
                    if (joy[0].button[0].b) { aPlayer.move(8); }
                    if (joy[0].button[2].b) { aPlayer.move(8); }
                    aPlayer.move(8);
                    aPlayer.update_prev();
                    }
        //Oh noes, the secret commands!
        else if (key[KEY_Z]) { stick[0].generateNewCoordinates(); }
        else if (key[KEY_X]) { aPlayer.incrementTotalPicked(); }
        else if (key[KEY_C]) { aPlayer.increaseSpeed(); }
        else if (key[KEY_ESC] || key[KEY_F4]) { done = true; }

        if (thereIsCollision(aPlayer, stick[0]))
        {
          //play_sample(sample, volume, pan, frequency, loop)
          //http://oregonstate.edu/~barnesc/quick_reference.html
          //is your friend.
          play_sample(samScore, 255, 128, 1000, 0);
          stick[0].generateNewCoordinates();
          aPlayer.incrementTotalPicked();
          if ( aPlayer.TotalPicked() % 10 == 0 )
          {
              song_frequency += 25;
              stop_sample(samSong);
              play_sample(samSong, 255, 128, song_frequency, true);
              aPlayer.increaseSpeed();
          }
        }
        /*  Here lies stick #2, it lived a short and hellish life. :D
        else if (thereIsCollision(aPlayer, stick[1]))
        {
          stick[1].generateNewCoordinates(1);
          aPlayer.incrementTotalPicked();
        }
        */
      }
      speed_counter--;
    }//while (speed_counter > 0)

    if (iScreen == 1)       //The main menu is just a picture ;P
    {
      draw_sprite(buffer, bmpMainMenu, 0,0);
    }
    else if (iScreen == 2)  //The about screen is two pictures!
    {
      draw_sprite(buffer, bmpMainMenu, 0,0);
      draw_sprite(buffer, bmpAbout, (buffer->w / 2 - bmpAbout->w / 2) , (buffer->h / 2 - bmpAbout->h / 2) );
    }
    else if (iScreen == 3)  //The main game's output... I should put this in a function...!
    {
      draw_sprite(buffer, bmpGameBg, 0, 0);
      textprintf(buffer,font,2,2,makecol(255,255,255), "Sticks Collected: %i", aPlayer.TotalPicked() );
      if ( aPlayer.Speed() == 1 )
      {
          textprintf(buffer,font,200,2,makecol(255,255,255), "Speed: %0.0f mph", aPlayer.Speed() );
      }
      else
      {
          textprintf(buffer,font,200,2,makecol(255,255,255), "Speed: %0.0f mph", aPlayer.Speed()*7 );
      }
      textprintf(buffer,font,408,2,makecol(255,255,255), "Rank: ");
      if (aPlayer.TotalPicked() < 10) { textprintf(buffer,font,452,2,makecol(255,255,255), "Nubsaws"); }
      else if (aPlayer.TotalPicked() >= 10 && aPlayer.TotalPicked() < 20) { textprintf(buffer,font,452,2,makecol(255,255,255), "Amateur"); }
      else if (aPlayer.TotalPicked() >= 20 && aPlayer.TotalPicked() < 30) { textprintf(buffer,font,452,2,makecol(255,200,0), "Stick Picker"); }
      else if (aPlayer.TotalPicked() >= 30 && aPlayer.TotalPicked() < 40) { textprintf(buffer,font,452,2,makecol(0,200,255), "Mr. Awesome"); }
      else if (aPlayer.TotalPicked() >= 40 && aPlayer.TotalPicked() < 50) { textprintf(buffer,font,452,2,makecol(175,130,0), "Over the Hill"); }
      else if (aPlayer.TotalPicked() >= 50) { textprintf(buffer,font,452,2,makecol(255,0,0), "No life"); }

      draw_sprite(buffer, bmpStick, stick[0].X(), stick[0].Y() );

      aPlayer.draw(buffer, bmpAlec);

      if (aPlayer.TotalPicked() == 10) { textprintf(buffer, font, aPlayer.X() - 110, aPlayer.Y() - 10,makecol(255,255,255), "Yay! Just infinite more to go!" ); }
      else if (aPlayer.TotalPicked() == 50)
      {
        textprintf(buffer, font, aPlayer.X() - 110, aPlayer.Y() - 17,makecol(255,0,0), "You should probably stop now." );
        textprintf(buffer, font, aPlayer.X() - 100, aPlayer.Y() - 10,makecol(255,0,0), "This game never ends." );
      }
    }

    acquire_screen();
    blit(buffer, screen, 0, 0, 0, 0, 640, 480);
    clear_bitmap(buffer);
    release_screen();
  }//while (!key[KEY_ESC])

  destroy_sample(samSong);
  destroy_sample(samScore);
  destroy_bitmap(bmpMainMenu);
  destroy_bitmap(buffer);

  return 0;
}
END_OF_MAIN();
