#pragma once

class Stick
{
  private:
  int x, y;
  public:
  int X();
  int Y();
  void generateNewCoordinates();
  Stick();
  ~Stick();
};
