~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~PICKIN' STICKS~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
By Rachel Morris
Website: http://www.eccentrix.com/members/cocoa/
Allegro: http://www.allegro.cc/members/RachelMorris

Made Dec 25 - Dec 27, 2006


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~   UPDATES    ~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Version 1.1 - Jan 5, 2008
- Music speeds up when you collect (score % 10 = 0) sticks
- Alec also speeds up every 10 sticks
- Added shitty ghost trail when you're going fast enough

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~     ABOUT    ~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
I suck at coming up with ideas for games, so oftentimes I go
to my friends for help.  One night, I was asking what
was a fun thing in 2D games, and my friend Alec said
"Picking up sticks.  Endlessly.  For no Raisin."
And there you have it.

This game is about Alec picking up sticks, endlessly, for a weird
fat guy, for no reward whatsoever.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~   CONTROLS   ~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

(in game)
Arrow Keys to move
W - Changed to windowed mode
F - Change to fullscreen mode
ESC - Exit

If you go into the code you can un-comment out:
Z - Generate new coordinates for the stick
X - Add 1 to your score

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~ WHO DID WHAT ~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Programming - Rachel Morris
Graphics - Rachel Morris
Music - RPG Maker
Sound effect - RPG Maker
Game "plot" - Alec Hansen :P

Programmed in Code::Blocks with the 
Allegro game programming library

Graphics done in Paint Shop Pro

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
~~~~~~~~~~~~      FAQ     ~~~~~~~~~~~~
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
All of these questions really were asked to me!
(not)

Q: What's your high score?!
A: 12, legitly.

Q: Why's this game suck?
A: Becuase I just wanted to make a quick game.

Q: What's your favorite color?
A: Purple.

Q: I want to go buy a game at the gamestore.  What should I buy?
A: Psychonauts by Double Fine.